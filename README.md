# Slash GraphQL Spring Boot Example Application (`slash-graph-ql`)

[![pipeline status](https://gitlab.com/johnjvester/slash-graph-ql/badges/master/pipeline.svg)](https://gitlab.com/johnjvester/slash-graph-ql/commits/master)

> The Slash GraphQL Spring Boot Example Application (`slash-graph-ql`) is a [Java](<https://en.wikipedia.org/wiki/Java_(programming_language)>) based application, utilizing the [Spring Boot](<https://spring.io/projects/spring-boot>) framework and is intended to provide a working example of [Slash GraphQL](https://dgraph.io/slash-graphql) usage.

> In this example the [Dgraph Slash GraphQL](https://dgraph.io/slash-graphql) service is utilized to populate data into a [Slope One](https://en.wikipedia.org/wiki/Slope_One) ratings algorithm to act as a recommendation engine - based upon data found in Dgraph Slash GraphQL.

## Publications

This repository is related to a two-part series published on DZone.com:

* Part #1 - [Building an Amazon-Like Recommendation Engine Using Slash GraphQL](https://dzone.com/articles/building-a-recommendations-engine-using-spring-boo)
* Part #2 - [Connecting Angular to the Spring Boot and Slash GraphQL Recommendations Engine](https://dzone.com/articles/connecting-angular-to-the-spring-boot-and-slash-gr)

## Slash GraphQL Configuration

After creating a free Dgraph Slash GraphQL account, the following schema needs to be created:

```
type Artist {
    name: String! @id @search(by: [hash, regexp])
    ratings: [Rating] @hasInverse(field: about)
}

type Customer {
    username: String! @id @search(by: [hash, regexp])
    ratings: [Rating] @hasInverse(field: by)
}

type Rating {
    id: ID!
    about: Artist!
    by: Customer!
    score: Int @search
}
```

With the schema in place, a few example `Artist`s can be added using the API Explorer in Dgraph GraphQL:

```
mutation {
  addArtist(input: [
    {name: "Eric Johnson"},
    {name: "Genesis"},
    {name: "Led Zeppelin"},
    {name: "Rush"},
    {name: "Triumph"},
    {name: "Van Halen"},
    {name: "Yes"}]) {
    artist {
      name
    }
  }
}
```

Next, a base of example `Customer`s are added using the API Explorer in Dgraph GraphQL:

```
mutation {
  addCustomer(input: [
    {username: "Doug"},
    {username: "Jeff"},
    {username: "John"},
    {username: "Russell"},
    {username: "Stacy"}]) {
    customer {
      username
    }
  }
}
```

Finally, some `Rating`s data needs to be added into the rating object, using the Dgraph GraphQL API Explorer:

```
mutation {
  addRating(input: [{
    by: {username: "Jeff"},
    about: { name: "Triumph"},
    score: 4}])
  {
    rating {
      score
      by { username }
      about { name }
    }
  }
}
```

For this example, I populated the data as shown below:

![Example Matrix](./ExampleRatingsMatrix.png)


## Configuration Information

The following elements need to be set in order to run the `slash-graph-ql` application:

* `${SLASH_GRAPH_QL_HOSTNAME}` - GraphQL Endpoint (hostname) as shown in the Dgraph GraphQL Dashboard `[slash-graph-ql.hostname]`
* `${PORT}` - server port to use `[server.port]` 

## Getting a Recommendation

In order to use the Dgraph Slash GraphQL Spring Boot Example Application (`slash-graph-ql`) and data in GraphQL to make a recommendation, simply call the following URI:
 
`{spring-boot-service-host-name}/recommend`

Based upon the matrix of data provided, results similar to those noted below will be returned:

```
{
    "matchedCustomer": {
        "username": "Russell"
    },
    "recommendationsMap": {
        "Artist(name=Eric Johnson)": 0.7765842245950264,
        "Artist(name=Yes)": 0.7661904474477843,
        "Artist(name=Triumph)": 0.7518039724158979,
        "Artist(name=Van Halen)": 0.7635436007978691
    },
    "ratingsMap": {
        "Artist(name=Genesis)": 0.4,
        "Artist(name=Led Zeppelin)": 1.0,
        "Artist(name=Rush)": 0.6
    },
    "resultsMap": {
        "Artist(name=Eric Johnson)": 0.7765842245950264,
        "Artist(name=Genesis)": 0.4,
        "Artist(name=Yes)": 0.7661904474477843,
        "Artist(name=Led Zeppelin)": 1.0,
        "Artist(name=Rush)": 0.6,
        "Artist(name=Triumph)": 0.7518039724158979,
        "Artist(name=Van Halen)": 0.7635436007978691
    }
}
```

where:

* `matchedCustomer` is a random customer selected (using the [`random-generator`](https://gitlab.com/johnjvester/RandomGenerator) library) to match the current user
* `ratingsMap` is a summary of the ratings provided by that random customer
* `resultsMap` contains the results from the Slope One algorithm being populated for items not ranked by the random customer selected
* `recommendationsMap` is a copy of the `resultsMap` without the `ratingsMap` data, providing recommendations (and their Slope One weight) for the random customer

What this means is that a customer similar to `Russell` would most likely enjoy the music of `Eric Johnson`, based upon Russell's provided reviews and the computed reviews based upon the results of other customers.  Of course this is a very simple example of a recommendations engine - merely to illustrate how data in Dgraph GraphQL can be gathered and processed.

Please note - since the `Jeff` customer has reviewed all items, Slope One would have not computed any missing attributes.  In my matrix of ratings, this was by design to validate the functionality of the Slope One processor.

Made with ♥ by johnjvester@gmail.com, because I enjoy writing code.