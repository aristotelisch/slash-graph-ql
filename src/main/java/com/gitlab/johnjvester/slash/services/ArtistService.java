package com.gitlab.johnjvester.slash.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.johnjvester.slash.config.SlashGraphQlProperties;
import com.gitlab.johnjvester.slash.models.Artist;
import com.gitlab.johnjvester.slash.models.graphQL.SlashGraphQlResultArtist;
import com.gitlab.johnjvester.slash.utils.RestTemplateUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class ArtistService {
    private final SlashGraphQlProperties slashGraphQlProperties;

    private static final String ARTIST_QUERY = "query { queryArtist { name } }";

    public List<Artist> getAllArtists() throws Exception {
        ResponseEntity<String> responseEntity = RestTemplateUtils.query(slashGraphQlProperties.getHostname(), ARTIST_QUERY);

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            SlashGraphQlResultArtist slashGraphQlResult = objectMapper.readValue(responseEntity.getBody(), SlashGraphQlResultArtist.class);
            log.debug("slashGraphQlResult={}", slashGraphQlResult);
            return slashGraphQlResult.getData().getQueryArtist();
        } catch (JsonProcessingException e) {
            throw new Exception("An error was encountered processing responseEntity=" + responseEntity.getBody(), e);
        }
    }
}
